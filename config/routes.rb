Rails.application.routes.draw do
  root :to => "catalogs#index"
  resources :catalogs do
  	resources :products
  end
end
