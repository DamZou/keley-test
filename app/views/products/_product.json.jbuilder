json.extract! product, :id, :name, :code, :description, :price, :weight, :created_at, :updated_at
json.url product_url(product, format: :json)