json.extract! catalog, :id, :name, :code, :number_of_products, :created_at, :updated_at
json.url catalog_url(catalog, format: :json)