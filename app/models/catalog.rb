class Catalog < ApplicationRecord

	has_many :products, dependent: :destroy

	def quantity
		i = 0
		if self.products
			self.products.each do |prod|
				i = i + 1
			end
		end
		return i
	end
end
