class AddCatalogIdToProduct < ActiveRecord::Migration[5.0]
  def change
  	add_column :products, :catalog_id, :integer
  end
end
